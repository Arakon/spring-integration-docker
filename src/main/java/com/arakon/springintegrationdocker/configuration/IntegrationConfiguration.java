package com.arakon.springintegrationdocker.configuration;

import com.arakon.springintegrationdocker.entity.ModelDataEntity;
import com.arakon.springintegrationdocker.entity.ModelDataEntityOutput;
import com.arakon.springintegrationdocker.repository.ModelDataEntityOutputRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSelector;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.filter.MessageFilter;
import org.springframework.integration.jdbc.JdbcPollingChannelAdapter;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.messaging.MessageHandler;

import javax.transaction.Transactional;

/**
 * Created by arakon on 03.11.19.
 */
@Configuration
@EnableIntegration
public class IntegrationConfiguration {

    @Autowired
    private DriverManagerDataSource dataSource;
    @Autowired
    private ModelDataEntityOutputRepository modelDataEntityOutputRepository;

    @Bean
    public MessageSource<Object> jdbcMessageSource() {
        return new JdbcPollingChannelAdapter(dataSource, "SELECT * FROM DATA");
    }

    @Bean(name = PollerMetadata.DEFAULT_POLLER)
    public PollerMetadata poller() {
        return Pollers.fixedRate(500).maxMessagesPerPoll(1).get();
    }

    @Bean
    public MessageFilter nameFilter() {
        final MessageSelector selector = message -> {
            if (message.getPayload() instanceof ModelDataEntity) {
                ModelDataEntity entity = (ModelDataEntity) message.getPayload();
                final String name = entity.getName();
                return name != null && name.contains("name");
            }
            return false;
        };
        return new MessageFilter(selector);
    }

    @Bean
    @Transactional
    public MessageHandler dataHandler() {
        return message -> {
            final ModelDataEntityOutput r = (ModelDataEntityOutput) message.getPayload();
            modelDataEntityOutputRepository.save(r);
        };
    }

    @Bean
    public IntegrationFlow flow() {
        return IntegrationFlows.from(jdbcMessageSource())
                .filter(nameFilter())
                .handle(dataHandler())
                .get();
    }

}
