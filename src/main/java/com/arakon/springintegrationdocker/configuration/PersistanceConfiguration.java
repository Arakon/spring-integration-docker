package com.arakon.springintegrationdocker.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import lombok.extern.java.Log;

/**
 * Created by arakon on 13.10.19.
 */
@Log
@Configuration
@EnableJpaRepositories("com.arakon.springintegrationdocker.repository")
public class PersistanceConfiguration {

    private static final String DB_PASSWORD = "db.password";
    private static final String DB_USER = "db.username";
    private static final String DB_DRIVER_CLASS_NAME = "db.driverClassName";
    private static final String DB_URL = "db.url";
    public static final String CONFIG_FILE_NAME = "db.properties";

    @Bean
    DriverManagerDataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        final Properties properties = loadPropertiesFile();
        dataSource.setPassword(properties.getProperty(DB_PASSWORD));
        dataSource.setUsername(properties.getProperty(DB_USER));
        dataSource.setDriverClassName(properties.getProperty(DB_DRIVER_CLASS_NAME));
        dataSource.setUrl(properties.getProperty(DB_URL));
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("com.arakon.springintegrationdocker.entity");
        em.setJpaDialect(new HibernateJpaDialect());
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(jpaProperties());
        return em;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Bean
    public Properties jpaProperties() {
        final Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL94Dialect");
        return properties;
    }

    @Bean
    public Properties loadPropertiesFile() {
        final Properties prop = new Properties();
        try (InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(CONFIG_FILE_NAME)) {
            prop.load(resourceAsStream);
        } catch (IOException e) {
            System.err.println("Unable to load properties file : " + CONFIG_FILE_NAME);
        }
        return prop;
    }

}
