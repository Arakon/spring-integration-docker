package com.arakon.springintegrationdocker.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.arakon.springintegrationdocker.service.DataService;
import com.arakon.springintegrationdocker.service.impl.DataServiceImpl;

/**
 * Created by arakon on 13.10.19.
 */
@Configuration
@Import({PersistanceConfiguration.class, IntegrationConfiguration.class})
@ComponentScan("com.arakon.springintegrationdocker")
@EnableWebMvc
public class AppConfiguration {

    @Bean
    public String name() {
        return "Spring MVC + Spring Integration + RabbitMQ + PostgresQl";
    }
}
