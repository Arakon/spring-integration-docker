package com.arakon.springintegrationdocker.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by arakon on 03.11.19.
 */
@Entity
@Data
@Table(name = "OUTPUT")
public class ModelDataEntityOutput {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "data_id_generator")
    @SequenceGenerator(name="data_id_generator", sequenceName = "data_id_sequence")
    private Long id;
    private String name;
    private Timestamp timestamp;
}
