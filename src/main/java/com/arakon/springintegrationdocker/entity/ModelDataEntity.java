package com.arakon.springintegrationdocker.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by arakon on 13.10.19.
 */
@Entity
@Table(name = "DATA")
@Data
public class ModelDataEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "data_id_generator")
    @SequenceGenerator(name="data_id_generator", sequenceName = "data_id_sequence")
    private Long id;
    private String name;
}
