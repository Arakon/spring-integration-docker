package com.arakon.springintegrationdocker.controller;

import java.util.List;

import com.arakon.springintegrationdocker.entity.ModelDataEntityOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.arakon.springintegrationdocker.entity.ModelDataEntity;
import com.arakon.springintegrationdocker.service.DataService;

@RestController
@RequestMapping("entities")
public class ModelDataController {

	@Autowired
	private DataService dataService;

	@GetMapping
	public List<ModelDataEntity> getModelDataEntities() {
		return dataService.getModelDataEntities();
	}

	@GetMapping("{id}")
	public ModelDataEntity getEntity(@PathVariable Long id) {
		return dataService.getEntity(id);
	}

	@PostMapping
	public void saveModelDataEntity(@RequestBody ModelDataEntity entity) {
		dataService.addModelDataEntity(entity);
	}

	@GetMapping("output")
	public List<ModelDataEntityOutput> getOutputEntities() {
		return dataService.getOutputEntity();
	}
}
