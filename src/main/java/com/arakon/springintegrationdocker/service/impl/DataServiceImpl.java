package com.arakon.springintegrationdocker.service.impl;

import com.arakon.springintegrationdocker.entity.ModelDataEntity;
import com.arakon.springintegrationdocker.entity.ModelDataEntityOutput;
import com.arakon.springintegrationdocker.repository.ModelDataEntityOutputRepository;
import com.arakon.springintegrationdocker.repository.ModelDataEntityRepository;
import com.arakon.springintegrationdocker.service.DataService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DataServiceImpl implements DataService {

	private final ModelDataEntityRepository modelDataEntityRepository;
	private final ModelDataEntityOutputRepository modelDataEntityOutputRepository;

	@Override
	@Transactional
	public void addModelDataEntity() {
		final ModelDataEntity entity = new ModelDataEntity();
		entity.setName("TestName");
		modelDataEntityRepository.save(entity);
	}

	@Override
	@Transactional
	public void addModelDataEntity(ModelDataEntity entity) {
		modelDataEntityRepository.save(entity);
	}

	@Override
	public List<ModelDataEntity> getModelDataEntities() {
		final ArrayList<ModelDataEntity> result = new ArrayList<>();
		modelDataEntityRepository.findAll().forEach(result::add);
		return result;
	}

	@Override
	public ModelDataEntity getEntity(Long id) {
		final Optional<ModelDataEntity> result = modelDataEntityRepository.findById(id);
		return result.orElse(null);
	}

	@Override
	public List<ModelDataEntityOutput> getOutputEntity() {
		final ArrayList<ModelDataEntityOutput> result = new ArrayList<>();
		modelDataEntityOutputRepository.findAll().forEach(result::add);
		return result;
	}
}
