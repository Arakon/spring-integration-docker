package com.arakon.springintegrationdocker.service;

import java.util.List;

import com.arakon.springintegrationdocker.entity.ModelDataEntity;
import com.arakon.springintegrationdocker.entity.ModelDataEntityOutput;

public interface DataService {
	void addModelDataEntity();

	void addModelDataEntity(ModelDataEntity entity);

	List<ModelDataEntity> getModelDataEntities();

	ModelDataEntity getEntity(Long id);

	List<ModelDataEntityOutput> getOutputEntity();
}
