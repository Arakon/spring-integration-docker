package com.arakon.springintegrationdocker.repository;

import org.springframework.data.repository.CrudRepository;

import com.arakon.springintegrationdocker.entity.ModelDataEntity;

public interface ModelDataEntityRepository extends CrudRepository<ModelDataEntity, Long> {
}
