package com.arakon.springintegrationdocker.repository;

import com.arakon.springintegrationdocker.entity.ModelDataEntityOutput;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by arakon on 03.11.19.
 */
public interface ModelDataEntityOutputRepository extends CrudRepository<ModelDataEntityOutput, Long> {
}
